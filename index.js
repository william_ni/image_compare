var ref = require('ref');
var ffi = require('ffi');
var jpeg_js = require("jpeg-js")
var fs = require("fs")

//float iqa_ms_ssim(const unsigned char *ref, const unsigned char *cmp, int w, int h, int stride, const struct iqa_ms_ssim_args *args);

var libmytest = ffi.Library('libiqa', {
    'iqa_ms_ssim': [ 'float', [ 'pointer', 'pointer', 'int', 'int', 'int', 'pointer'] ]
});


function decodeJPEG(fileName){
    var jpegData = fs.readFileSync(fileName);
    var rawImage = jpeg_js.decode(jpegData);
    return rawImage;
}

var image_ios = decodeJPEG("collage_ios.jpg");
var image_android = decodeJPEG("collage_android.jpg");

var quality = libmytest.iqa_ms_ssim(image_ios.data, image_android.data, image_ios.width, image_ios.height, image_ios.width*4, null);

console.log(quality);


//var out = libmytest.mytest(20, 88);
//console.log(out);